<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->Uuid('id');
            $table->boolean('like')->default(true);
            $table->foreignUuid('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->uuidMorphs('likeable');
            $table->timestamps();
            $table->softDeletes();
     });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
