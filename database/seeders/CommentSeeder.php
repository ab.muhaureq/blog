<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;

class CommentSeeder extends Seeder
{
    public $comments;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::select('id')->get();
        $users = User::select('id')->get();
        for ($i = 0; $i < 100; $i++) {
            $comment = Comment::factory()->make()->toArray();
            $comment['user_id'] = $users->random()->id;
            $comment['post_id'] = $posts->random()->id;
            $this->comments[] = $comment;
        }
        Comment::insert($this->comments);
    }
}
