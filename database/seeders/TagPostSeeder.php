<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Post;
use Illuminate\Support\Facades\DB;


class TagPostSeeder extends Seeder
{
    public $insert_tags = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::select('id')->get();
        $tags =  Tag::select('id')->get();
        foreach ($posts as $post) {
            $tags_ids = $tags->random(random_int(1, 3))->pluck('id');
            foreach ($tags_ids as $tag_id)
                $this->insert_tags[] = [
                    'post_id' => $post->id,
                    'tag_id' => $tag_id,
                ];
        }
        //dd($insert_tags);
        DB::table('post_tag')->insert($this->insert_tags);
    }
}
