<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Like;

class LikeSeeder extends Seeder
{
    public $likes=[];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::select('id')->get();
        $posts = Post::select('id')->get();
        $comments = Comment::select('id')->get();


        foreach($posts as $post){
            $posts_like = Like::factory()->make()->toArray();
            $posts_like['user_id'] = $users->random()->id;
            $posts_like['likeable_id'] = $post->id;
            $posts_like['likeable_type'] =Post::class;
            $this->likes[] = $posts_like;
        }

        foreach($comments as $comment){
            $comments_like = Like::factory()->make()->toArray();
            $comments_like['user_id'] = $users->random()->id;
            $comments_like['likeable_id'] = $comment->id;
            $comments_like['likeable_type'] =Comment::class;
            $this->likes[] = $comments_like;

        }
        Like::insert($this->likes);
    }
}
