<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;


class PostSeeder extends Seeder
{
    public $posts;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::select('id')->get();
        $users = User::select('id')->get();
        for ($i = 0; $i < 100; $i++) {
            $post = Post::factory()->make()->toArray();
            $post['user_id'] = $users->random()->id;
            $post['category_id'] = $categories->random()->id;
            $this->posts[] = $post;
        }
        Post::insert($this->posts);
    }
}
