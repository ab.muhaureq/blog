<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Categories = Category::factory()->times(20)->make([])->toArray();
        //Category::insert($Categories);
        Category::insert($Categories);

    }

    }
