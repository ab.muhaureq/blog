<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\Category;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' =>$this->faker->uuid,
            'user_id' =>null,
            'title' => $this->faker->sentence($nbWords = random_int(3,5)),
            'summary'=> $this->faker->sentence($nbWords = random_int(6,20)),
            'slug'=> $this->faker->sentence($nbWords = random_int(6,20)),
            'context' => $this->faker->text($maxNbChars  = random_int(50,200)),
            'category_id' => null
        ];
    }
}
