<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\User;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Traits\Uuids;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' =>$this->faker->uuid,
            'user_id' => null,
            'post_id' => null,
            'title' => $this->faker->sentence($nbWords = random_int(1,3)),
            'context' => $this->faker->sentence($nbWords = random_int(3,15))
            //
        ];
    }
}
