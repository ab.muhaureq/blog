# Laravel_Template

This is a template that we Bootfi uses to develop APIs.

It contains the following files and packages:

1. laravel/passport
2. barrrydvh/laravel-cors
3. barryvdh/laravel-ide-helper
4. app/ApiResponse.php -> handles json responses
5. app/Http/Middleware/ForceApiResponse.php to add json header to every request/response


How Files Structured: 
```
---app/
--------Http/
------------Controllers/
------------------------API/
----------------------------v1/ ->(for the current api version)
```


RouteServiceProvider changes:

```
/**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::middleware('api')
            ->prefix('v' . config('app.api_version'))
            ->namespace($this->namespace . '\\' . 'v' . env('APP_VERSION'),1)
            ->group(base_path('routes/api.php'));
    }
```
    
    
Environment Files Changes:  
    .env & .env.example have APP_VERSION field which is by default has the value of 1.
