<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'user_id' => ['exists:users,id','required'],
            'title' => ['string','required'],
            'summary' => ['string','required'],
            'context'=>['string','required'],
            'category_id'=> ['exists:categories,id','required'],
            'slug' =>'string',
        ];
    }
}
