<?php

namespace App\Http\Middleware;

use Closure;

class DeleteEmptyAndNullStrings
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach ($request->all() as $key => $value)
            if (empty($value) || $value == null)
                unset($request[$key]);
        return $next($request);
    }
}
