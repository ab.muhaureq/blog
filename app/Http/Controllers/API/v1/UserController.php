<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return new UserResource(User::with(['roles'])->get());
        return sendResponse(__('users.get_data'),new UserResource(User::with(['roles'])->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $user = User::make($request->validated())->toArray();
        $user['id'] = Str::uuid();
        $input['activation_token'] = Str::random(60);

        User::insert($user);

//        $user = User::create($request->validated());

        // return (new UserResource($user))
        //     ->response()
        //     ->setStatusCode(Response::HTTP_CREATED);

        return sendResponse(__('users.created_successfully'),new UserResource($user));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return sendResponse(__('users.get_data'),new UserResource($user->load(['roles'])));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->validated());
        return sendResponse(__('users.update_user'),new UserResource($user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        $user->delete();
        return sendResponse(__('users.delete_data'),new UserResource($user));
    }
}
