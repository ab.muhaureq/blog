<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Notifications\CommentNotification;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return sendResponse(__('comments.get_all_data'), CommentResource::make(Comment::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentRequest $request,$post)
    {
        $comment = Comment::make($request->validated())->toArray();
        $comment['id'] = Str::uuid();
        $comment['user_id'] = Auth::user()->id;
        $comment['post_id'] = $post;
        // Comment::insert($comment);
        Comment::insert($comment);
        
        // $postUserID
        // $postUserId = Post::findOrFail($post)->getAttribute('user_id');
        $postOwner = User::findOrFail(Post::findOrFail($post)->getAttribute('user_id'));
        $postOwner->notify(new CommentNotification($comment));
        return sendResponse(__('Comments.created_successfully'), CommentResource::make($comment));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
