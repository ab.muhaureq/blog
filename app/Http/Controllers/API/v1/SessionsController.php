<?php

namespace App\Http\Controllers\API\v1;

//use App\Http\Requests\ChangePassword;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Resources\LoginResource;
use App\Http\Resources\SignupResource;
//use App\Models\Comment;
use App\Models\Post;
use App\Models\Tag;
use App\Notifications\ChangePasswordSuccess;
use App\Notifications\SignupActivate;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SessionsController
{

    public function test(Request $request)
    {
        $posts = Post::select('id')->get();

        $inseart_tags = [];
        $tags =  Tag::select('id')->get();


        foreach ($posts as $post) {
            $tags_ids = $tags->random(random_int(1, 3))->pluck('id');
            foreach ($tags_ids as $tag_id)
                $inseart_tags[] = [
                    'post_id' => $post->id,
                    'tag_id' => $tag_id,
                ];
            // $post->tags()->sync ($tagIds);
        }
        DB::table('post_tag')->insert($inseart_tags);
        return 'ok';
    }

    /**
     * User login api => username and password required
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        // $validator = Validator::make($request->all(), [
        //     'email' => 'required|email',
        //     'password' => 'required|min:5',
        // ]);

        // if ($validator->fails()) {
        //     return sendError($validator->errors()->first(), null, 401);
        // }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = Auth::user();

            // if (!$user->hasVerifiedEmail()) {

            //     return sendError(__('auth.verify_email'), null, 403);
            // }

            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();

            $user['tokenResult'] = $tokenResult;

            return sendResponse(__('auth.success_login'), LoginResource::make($user));
        }

        return sendError(__('auth.failed'), null, 401);
    }

    /**
     * User logout api => accessToken  required
     *
     * @return 204
     */

    public function logout()
    {
        $token = auth()->user()->getRememberToken();
        //$token->revoke();

        return sendResponse(__('auth.logout'), null, 204);
    }

    public function register(RegistrationRequest $request)
    {
        $input = $request->only((new User())->getFillable());
        $input['password'] = Hash::make($request->password);
        $input['activation_token'] = Str::random(60);

        $user = User::create($input);

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        $user['tokenResult'] = $tokenResult;
        //$user->notify(new SignupActivate($user));

        return sendResponse(__('auth.success_register'), SignupResource::make($user));
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();
        $user->notify(new ChangePasswordSuccess());
        return sendResponse(__('passwords.reset'));
    }

    public function resendActivationEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user)
            return sendError(__('passwords.user'), null, 404);
        elseif ($user->active)
            return sendError(__('users.account_already_active'), null, 422);

        $user->notify(new SignupActivate($user));
        return sendResponse(__('users.resend_activation_email'), null);
    }

    public function activateAccount($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return sendError(__('auth.invalid_token'));
        }
        $user->active = true;
        $user->markEmailAsVerified();
        $user->activation_token = '';
        $user->save();
        $data = $user->only(['name', 'email']);
        return sendResponse(__('auth.email_verified'), $data);
    }
}
