<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Auth::user()->posts()->with('')->orderBy()->get();
        //request()->user()->posts()->with('')->orderBy()->get();
        return sendResponse(__('posts.get_all_data'), PostResource::make(Post::with(['user', 'comments'])->simplePaginate()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $post = Post::make($request->validated())->toArray();
        $post['id'] = Str::uuid();
        $post['user_id'] = Auth::user()->id;
        $post['slug'] = $request->get('slug');
        Post::insert($post);
        return sendResponse(__('posts.created_successfully'), PostResource::make($post));
    }

    /**1
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return sendResponse(__('posts.get_data'), PostResource::make($post->load(['user', 'category', 'tags', 'likes', 'comments'])));
        // return sendResponse(__('posts.get_data'),new PostResource($post->loadCount('comments')));
    }

    public function searchPost(Request $request)
    {
        $name = $request->name;
        $result = Post::where('title', 'like', "{$name}%")->get();
        return sendResponse(__('posts.get_data'), PostResource::make($result));
    }

    public function postTags(Request $request)
    {
        $post = Post::findOrFail($request->get('id'));
        return sendResponse(__('posts.get_data'), PostResource::make($post->load(['tags'])));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, Post $post)
    {
        $post->update($request->validated());

        return sendResponse(__('posts.update_post'), PostResource::make($post));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return sendResponse(__('posts.delete_data'), PostResource::make($post));
    }
}
