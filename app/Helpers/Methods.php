<?php

use \Illuminate\Http\Request;

function unsetEmptyParams(Request $request)
{
    foreach ($request->all() as $key => $value)
        if (empty($value) || $value == null)
            unset($request[$key]);
}
