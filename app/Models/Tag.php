<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Traits\Uuids;

class Tag extends Model
{
    use HasFactory,Uuids;


    protected $table = 'tags';
    protected $keyType = 'string';


    public $incrementing = false;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'context',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function posts(){
        return $this->BelongsToMany(Post::class,'post_tag','tag_id','post_id');
    }
}
