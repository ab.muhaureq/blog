<?php
return array(
    'created_successfully' => 'تم التسجيل بنجاح.',
    'get_data' => 'تم جلب البيانات بنجاح.',
    'resend_activation_email' => 'تم ارسال بريد التفعيل بنجاح.',
    'account_already_active' => 'حساب مفعل مسبقا.',
    'update_user'=>'تم تعديل بيانات المستخدم',
    'delete_data' => 'تم حذف المستخدم'
);
