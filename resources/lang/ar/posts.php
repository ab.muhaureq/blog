<?php
return array(
    'created_successfully' => 'تم التسجيل بنجاح.',
    'get_data' => 'تم جلب البيانات بنجاح.',
    'get_all_data' => 'تم جلب المقالات بنجاح.',
    'update_post'=>'تم تعديل المقال بنجاح',
    'delete_post' => ' تم حذف المقال بنجاح'
);
