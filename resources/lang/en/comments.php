<?php


return [
    'created_successfully' => 'Comment created successfully.',
    'get_data' => 'Comment fetched successfully.',
    'get_all_data' => 'Comments fetched successfully.',
    'update_Comment'=>'Comment updated successfully',
    'delete_data' => 'Comments deleted successfully'
];
