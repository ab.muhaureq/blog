@servers(['web' => 'ubuntu@34.212.164.242'])

@setup

/*
|--------------------------------------------------------------------------
| Git Config
|--------------------------------------------------------------------------
|
| The git repository location.
|
*/

$repository = ' git@gitlab.com:fh32000/laundry_app.git'; //configure the repo uri
$branch = isset($branch) ? $branch : "staging";

/*
|--------------------------------------------------------------------------
| Server Paths
|--------------------------------------------------------------------------
|
| The base paths where the deployment will happens.
|
*/

$releases_dir = '/var/www/html/staging_laundry_app/releases';
$app_dir = '/var/www/html/staging_laundry_app';
$release = date('YmdHis');
$new_release_dir = $releases_dir .'/'. $release;

/*
|--------------------------------------------------------------------------
| Num of releases
|--------------------------------------------------------------------------
|
| The number of releases to keep.
|
*/

$keep = 3;

@endsetup

@story('deploy')
clone_repository
run_composer
update_symlinks
setup_app
clean
succeed
@endstory

@task('clone_repository')
echo 'Cloning repository'
[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
git clone --depth 1 --branch {{ $branch }} {{ $repository }} {{ $new_release_dir }}
cd {{ $new_release_dir }}
git reset --hard {{ $commit }}
@endtask

@task('run_composer')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir }}
composer install --quiet --prefer-dist --optimize-autoloader
composer dumpautoload
@endtask

@task('update_symlinks')
echo "Linking storage directory"
rm -rf {{ $new_release_dir }}/storage
ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('setup_app')
echo "Setting up the app"
cd {{ $new_release_dir }}
php artisan storage:link
php artisan key:generate
php artisan migrate:refresh --force --seed
php artisan cache:clear
php artisan config:clear
php artisan passport:install
@endtask

{{-- Clean old releases --}}
@task('clean')
echo "Clean old releases";
cd {{ $releases_dir }};
sudo rm -rf $(ls -t | tail -n +{{ $keep }});
@endtask

@task('succeed')
echo 'Deployment completed successfully.'
@endtask
