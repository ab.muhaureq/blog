<?php

/**
 * @file
 * Description of what this module (or file) is doing.
 */

/**
 * Implements hook_help().
 */

use App\Http\Controllers\API\v1\AvatarController;
use App\Http\Controllers\API\v1\CommentController;
use App\Http\Controllers\API\v1\LikeController;
use App\Http\Controllers\API\v1\NotificationController;
use App\Http\Controllers\API\v1\PostController;
use App\Http\Controllers\API\v1\SessionsController;
use App\Http\Controllers\API\v1\UserController;

Route::name('sessions.')->group(function () {
Route::post('login', [SessionsController::class, 'login'])->name('login');
Route::get('test', [SessionsController::class, 'test'])->name('test');
Route::get('searchPost', [PostController::class, 'searchPost'])->name('searchPost');
//Route::get('users', [UserController::class, 'index'])->name('users');
Route::apiResource('avatar',AvatarController::class);
Route::get('/send-notification', [NotificationController::class, 'sendOfferNotification']);

Route::name('admin')->middleware('auth:api')->group(function () {

    Route::apiResource('users',UserController::class);
    Route::apiResource('posts',PostController::class);
    Route::apiResource('comments',CommentController::class);
    Route::post('posts/{post}/addComment',[CommentController::class,'store']);
    Route::post('posts/{post}/addLike',[LikeController::class,'likePost']);
    Route::post('posts/{post}/unLike',[LikeController::class,'unlikePost']);

});
Route::post('register', [SessionsController::class, 'register'])->name('register');
Route::get('register/activate/{token}', [SessionsController::class, 'activateAccount'])->name('activate-account');
Route::post('register/resend-activate', [SessionsController::class, 'resendActivationEmail'])->name('resend-activation-email');

/**
 * Here we put protected routes
 */
Route::middleware('auth:api')->group(function () {
    Route::post('password/change', [SessionsController::class, 'changePassword'])->name('change-password');
    Route::post('logout', [SessionsController::class, 'logout'])->name('logout');
});
});
